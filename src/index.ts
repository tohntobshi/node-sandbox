import { json } from "body-parser";
import * as express from "express";
import * as CONFIG from "./config";
import * as controllers from "./controllers";
import { prepareDB } from "./db";

async function startUp() {
  try {
    await prepareDB();
  } catch (e) {
    console.log("database problem");
    process.exit(1);
  }
  const app = express();
  app.use(json(), controllers.bodyParserErrorHandler);
  app.get("/get_me", controllers.auth, controllers.getMe);
  app.post("/log_in", controllers.logIn);
  app.post("/sign_up", controllers.signUp);
  app.use(controllers.centralErrorHandler);
  app.listen(CONFIG.PORT, () => console.log(`listening on ${CONFIG.PORT}`));
}

startUp();

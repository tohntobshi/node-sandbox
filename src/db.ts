import * as mysql from "mysql";
import * as CONFIG from "./config";

export async function queryToPromise(connection: mysql.Connection, options: mysql.QueryOptions): Promise<any> {
  return new Promise((resolve, reject) => {
    connection.query(options, (err, results, fields) => {
      if (err) { reject(err); } else { resolve(results); }
    });
  });
}

export async function getConnection(additionalOptions?: { multipleStatements?: boolean }): Promise<mysql.Connection> {
  const dbOptions = {
    database: CONFIG.DB_NAME,
    host: CONFIG.DB_HOST,
    multipleStatements: additionalOptions && additionalOptions.multipleStatements ? true : false,
    password: CONFIG.DB_PASSWORD,
    user: CONFIG.DB_USER,
  };
  const conn = mysql.createConnection(dbOptions);
  await new Promise((resolve, reject) => {
    conn.connect((err) => {
      if (err) { reject(err); } else { resolve(); }
    });
  });
  return conn;
}

export async function prepareDB(): Promise<void> {
  const conn = await getConnection({ multipleStatements: true });
  const sql = `CREATE TABLE IF NOT EXISTS users (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(32),
    email VARCHAR(32),
    password VARCHAR(128)
  )`;
  await queryToPromise(conn, { sql });
  conn.end();
}

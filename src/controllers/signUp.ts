import { NextFunction, Request, Response } from "express";
import * as joi from "joi";
import { getConnection } from "../db";
import { errors } from "../errors";
import * as models from "../models";

export async function signUp(req: Request, res: Response, next: NextFunction) {
  try {
    const schema = {
      email: joi.string().email().required(),
      password: joi.string().required().min(4).max(16),
      username: joi.string().alphanum().required().min(4).max(16),
    };
    const { error, value } = joi.validate(req.body, schema);
    if (error) { throw new Error(errors.INVALID_INPUT); }
    const conn = await getConnection();
    if (await models.doesUserExist(conn, value.username)) { throw new Error(errors.USER_EXISTS); }
    await models.signUp(conn, value);
    res.status(200);
    res.end();
    // next();
  } catch (e) {
    next(e);
  }
}

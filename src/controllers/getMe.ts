import { NextFunction, Request, Response } from "express";
import * as joi from "joi";
import { getConnection, queryToPromise } from "../db";
import { AuthorizedRequest } from "../types";

export function getMe(req: AuthorizedRequest, res: Response, next: NextFunction) {
  res.send(req.session);
  next();
}

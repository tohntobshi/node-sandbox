import { NextFunction, Response } from "express";
import * as jwt from "jsonwebtoken";
import * as CONFIG from "../config";
import { errors } from "../errors";
import { AuthorizedRequest, Session } from "../types";

export function auth(req: AuthorizedRequest, res: Response, next: NextFunction) {
  try {
    const bearer = req.headers.authorization.split(" ")[1];
    const result = jwt.verify(bearer, CONFIG.JWT_SECRET);
    req.session = (result as Session);
    next();
  } catch (e) {
    next(new Error(errors.UNAUTHORIZED));
  }
}

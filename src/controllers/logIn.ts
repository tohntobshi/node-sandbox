import { NextFunction, Request, Response } from "express";
import * as joi from "joi";
import * as jwt from "jsonwebtoken";
import * as CONFIG from "../config";
import { getConnection } from "../db";
import { errors } from "../errors";
import * as models from "../models";

export async function logIn(req: Request, res: Response, next: NextFunction) {
  try {
    const schema = {
      password: joi.string().required().min(4).max(16),
      username: joi.string().alphanum().required().min(4).max(16),
    };
    const { error, value } = joi.validate(req.body, schema);
    if (error) { throw new Error(errors.INVALID_INPUT); }
    const conn = await getConnection();
    const { username, email } = await models.logIn(conn, value);
    if (username === "") { throw new Error(errors.UNAUTHORIZED); }
    const token = jwt.sign({ username, email }, CONFIG.JWT_SECRET, {
      expiresIn: "2 days",
    });
    res.send({ token });
    next();
  } catch (e) {
    next(e);
  }
}

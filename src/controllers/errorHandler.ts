import { NextFunction, Request, Response } from "express";
import { errors } from "../errors";

export function bodyParserErrorHandler(err: Error, req: Request, res: Response, next: NextFunction) {
  next(new Error(errors.INVALID_INPUT));
}

export function centralErrorHandler(err: Error, req: Request, res: Response, next: NextFunction) {
  switch (err.message) {
    case errors.INVALID_INPUT:
      res.status(400);
      break;
    case errors.UNAUTHORIZED:
      res.status(401);
      break;
    case errors.USER_EXISTS:
      res.status(409);
      break;
    default:
      res.status(500);
  }
  res.end();
}

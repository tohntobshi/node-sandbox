import { Request } from "express";

export interface Session {
  username: string;
  email: string;
}

export interface AuthorizedRequest extends Request {
  session: Session;
}

export interface SignUpForm {
  username: string;
  password: string;
  email: string;
}

export interface LogInForm {
  username: string;
  password: string;
}

import * as bcrypt from "bcrypt";
import { Connection } from "mysql";
import { queryToPromise } from "../db";
import { LogInForm, SignUpForm } from "../types";

export async function doesUserExist(conn: Connection, username: string): Promise<boolean> {
  const sql = `SELECT id FROM users WHERE username = ?`;
  const result = await queryToPromise(conn, { sql, values: [username] });
  if (result.length !== 0) { return true; } else { return false; }
}

export async function signUp(conn: Connection, form: SignUpForm): Promise<void> {
  const sql = `INSERT INTO users (username, password, email) VALUES (?, ?, ?)`;
  const hashedPwd = await bcrypt.hash(form.password, 8);
  await queryToPromise(conn, { sql, values: [form.username, hashedPwd, form.email] });
}

export async function logIn(conn: Connection, form: LogInForm): Promise<{ username: string, email: string }> {
  const sql = `SELECT * FROM users WHERE username = ?`;
  const result = await queryToPromise(conn, { sql, values: [form.username] });
  if (result.length === 0) { return { username: "", email: "" }; }
  const { username, email, password } = result[0];
  if (!await bcrypt.compare(form.password, password)) { return { username: "", email: "" }; }
  return { username, email };
}
